﻿namespace SRWS1DialUtility
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.mainmenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.sRWS1DialUtilityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.leftKnobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lkAxisModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.lkSeqModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.lkIrFbModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.lkDisableButton = new System.Windows.Forms.ToolStripMenuItem();
            this.centerKnobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ckAxisModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ckSeqModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ckIrFbModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.ckDisableButton = new System.Windows.Forms.ToolStripMenuItem();
            this.rightKnobToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rkAxisModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.rkSeqModeButton = new System.Windows.Forms.ToolStripMenuItem();
            this.rkDisableButton = new System.Windows.Forms.ToolStripMenuItem();
            this.exitButton = new System.Windows.Forms.ToolStripMenuItem();
            this.v01ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainmenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(221, 78);
            this.label1.TabIndex = 0;
            this.label1.Text = "This window is useless\njust let the app do its magic in the tray.\n\n\nCopyright Jar" +
    "i Lohtari \nUses iRacing SDK Wrapper by Nick Thissen ";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "SRW S1 Dial Utility";
            this.notifyIcon1.Visible = true;
            // 
            // mainmenu
            // 
            this.mainmenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sRWS1DialUtilityToolStripMenuItem,
            this.v01ToolStripMenuItem,
            this.toolStripMenuItem2,
            this.leftKnobToolStripMenuItem,
            this.centerKnobToolStripMenuItem,
            this.rightKnobToolStripMenuItem,
            this.exitButton});
            this.mainmenu.Name = "mainmenu";
            this.mainmenu.Size = new System.Drawing.Size(162, 180);
            // 
            // sRWS1DialUtilityToolStripMenuItem
            // 
            this.sRWS1DialUtilityToolStripMenuItem.Name = "sRWS1DialUtilityToolStripMenuItem";
            this.sRWS1DialUtilityToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.sRWS1DialUtilityToolStripMenuItem.Text = "SRW S1 Dial App";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Enabled = false;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(161, 22);
            this.toolStripMenuItem2.Text = " ";
            // 
            // leftKnobToolStripMenuItem
            // 
            this.leftKnobToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lkAxisModeButton,
            this.lkSeqModeButton,
            this.lkIrFbModeButton,
            this.lkDisableButton});
            this.leftKnobToolStripMenuItem.Name = "leftKnobToolStripMenuItem";
            this.leftKnobToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.leftKnobToolStripMenuItem.Text = "Left knob";
            // 
            // lkAxisModeButton
            // 
            this.lkAxisModeButton.Name = "lkAxisModeButton";
            this.lkAxisModeButton.Size = new System.Drawing.Size(141, 22);
            this.lkAxisModeButton.Text = "Axis";
            // 
            // lkSeqModeButton
            // 
            this.lkSeqModeButton.Name = "lkSeqModeButton";
            this.lkSeqModeButton.Size = new System.Drawing.Size(141, 22);
            this.lkSeqModeButton.Text = "+/- mode";
            // 
            // lkIrFbModeButton
            // 
            this.lkIrFbModeButton.Name = "lkIrFbModeButton";
            this.lkIrFbModeButton.Size = new System.Drawing.Size(141, 22);
            this.lkIrFbModeButton.Text = "iRacing Fbox";
            // 
            // lkDisableButton
            // 
            this.lkDisableButton.Name = "lkDisableButton";
            this.lkDisableButton.Size = new System.Drawing.Size(141, 22);
            this.lkDisableButton.Text = "Disable";
            // 
            // centerKnobToolStripMenuItem
            // 
            this.centerKnobToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ckAxisModeButton,
            this.ckSeqModeButton,
            this.ckIrFbModeButton,
            this.ckDisableButton});
            this.centerKnobToolStripMenuItem.Name = "centerKnobToolStripMenuItem";
            this.centerKnobToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.centerKnobToolStripMenuItem.Text = "Center knob";
            // 
            // ckAxisModeButton
            // 
            this.ckAxisModeButton.Name = "ckAxisModeButton";
            this.ckAxisModeButton.Size = new System.Drawing.Size(152, 22);
            this.ckAxisModeButton.Text = "Axis";
            // 
            // ckSeqModeButton
            // 
            this.ckSeqModeButton.Name = "ckSeqModeButton";
            this.ckSeqModeButton.Size = new System.Drawing.Size(152, 22);
            this.ckSeqModeButton.Text = "+/- mode";
            // 
            // ckIrFbModeButton
            // 
            this.ckIrFbModeButton.Name = "ckIrFbModeButton";
            this.ckIrFbModeButton.Size = new System.Drawing.Size(152, 22);
            this.ckIrFbModeButton.Text = "iRacing Fbox";
            // 
            // ckDisableButton
            // 
            this.ckDisableButton.Name = "ckDisableButton";
            this.ckDisableButton.Size = new System.Drawing.Size(152, 22);
            this.ckDisableButton.Text = "Disable";
            // 
            // rightKnobToolStripMenuItem
            // 
            this.rightKnobToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.rkAxisModeButton,
            this.rkSeqModeButton,
            this.rkDisableButton});
            this.rightKnobToolStripMenuItem.Name = "rightKnobToolStripMenuItem";
            this.rightKnobToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.rightKnobToolStripMenuItem.Text = "Right knob";
            // 
            // rkAxisModeButton
            // 
            this.rkAxisModeButton.Name = "rkAxisModeButton";
            this.rkAxisModeButton.Size = new System.Drawing.Size(152, 22);
            this.rkAxisModeButton.Text = "Axis";
            // 
            // rkSeqModeButton
            // 
            this.rkSeqModeButton.Name = "rkSeqModeButton";
            this.rkSeqModeButton.Size = new System.Drawing.Size(152, 22);
            this.rkSeqModeButton.Text = "+/- mode";
            // 
            // rkDisableButton
            // 
            this.rkDisableButton.Name = "rkDisableButton";
            this.rkDisableButton.Size = new System.Drawing.Size(152, 22);
            this.rkDisableButton.Text = "Disable";
            // 
            // exitButton
            // 
            this.exitButton.Name = "exitButton";
            this.exitButton.Size = new System.Drawing.Size(161, 22);
            this.exitButton.Text = "Exit";
            // 
            // v01ToolStripMenuItem
            // 
            this.v01ToolStripMenuItem.Enabled = false;
            this.v01ToolStripMenuItem.Name = "v01ToolStripMenuItem";
            this.v01ToolStripMenuItem.Size = new System.Drawing.Size(161, 22);
            this.v01ToolStripMenuItem.Text = "v. 0.1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(224, 96);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.Text = "S1 Dial App";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mainmenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip mainmenu;
        private System.Windows.Forms.ToolStripMenuItem leftKnobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lkAxisModeButton;
        private System.Windows.Forms.ToolStripMenuItem lkSeqModeButton;
        private System.Windows.Forms.ToolStripMenuItem lkIrFbModeButton;
        private System.Windows.Forms.ToolStripMenuItem lkDisableButton;
        private System.Windows.Forms.ToolStripMenuItem centerKnobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ckAxisModeButton;
        private System.Windows.Forms.ToolStripMenuItem ckSeqModeButton;
        private System.Windows.Forms.ToolStripMenuItem ckIrFbModeButton;
        private System.Windows.Forms.ToolStripMenuItem ckDisableButton;
        private System.Windows.Forms.ToolStripMenuItem rightKnobToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rkAxisModeButton;
        private System.Windows.Forms.ToolStripMenuItem rkSeqModeButton;
        private System.Windows.Forms.ToolStripMenuItem rkDisableButton;
        private System.Windows.Forms.ToolStripMenuItem exitButton;
        private System.Windows.Forms.ToolStripMenuItem sRWS1DialUtilityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem v01ToolStripMenuItem;
    }
}

