﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SRWS1DialUtility
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.notifyIcon1.ContextMenuStrip = this.mainmenu;
            this.lkAxisModeButton.Click += LkAxisModeButton_Click;
            this.lkSeqModeButton.Click += LkSeqModeButton_Click;
            this.lkIrFbModeButton.Click += LkIrFbModeButton_Click;
            this.lkDisableButton.Click += LkDisableButton_Click;
            this.ckAxisModeButton.Click += CkAxisModeButton_Click;
            this.ckSeqModeButton.Click += CkSeqModeButton_Click;
            this.ckIrFbModeButton.Click += CkIrFbModeButton_Click;
            this.ckDisableButton.Click += CkDisableButton_Click;
            this.rkAxisModeButton.Click += RkAxisModeButton_Click;
            this.rkSeqModeButton.Click += RkSeqModeButton_Click;
            this.rkDisableButton.Click += RkDisableButton_Click;
            this.exitButton.Click += ExitButton_Click;

            string lol = null;
            int l = lol.Length;

            runPreStartup();

        }

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Program.killApplication();
        }

        private void RkDisableButton_Click(object sender, EventArgs e)
        {
            Program.knobs.RightMode = "disabled";
            this.rkDisableButton.Text = "[x] " + this.rkDisableButton.Text;
            Program.saveConfig();
        }

        private void RkSeqModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.RightMode = "seq";
            this.rkSeqModeButton.Text = "[x] " + this.rkSeqModeButton.Text;
            this.rkAxisModeButton.Text = "Axis";
            this.rkDisableButton.Text = "Disable";
            Program.saveConfig();
        }

        private void RkAxisModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.RightMode = "axis";
            this.rkSeqModeButton.Text = "+/- mode";
            this.rkAxisModeButton.Text = "[x] " + this.rkAxisModeButton.Text;
            this.rkDisableButton.Text = "Disable";
            Program.saveConfig();
        }

        private void CkDisableButton_Click(object sender, EventArgs e)
        {
            Program.knobs.CenterMode = "disabled";
            this.rkAxisModeButton.Text = "Axis";
            this.rkSeqModeButton.Text = "+/- mode";
            this.ckDisableButton.Text = "[x] " + this.ckDisableButton.Text;
            this.ckIrFbModeButton.Text = "iRacing Fbox";
            Program.saveConfig();

        }

        private void CkIrFbModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.CenterMode = "irbb";
            this.ckAxisModeButton.Text = "Axis";
            this.ckDisableButton.Text = "Disable";
            this.ckIrFbModeButton.Text = "[x] " + this.ckIrFbModeButton.Text;
            this.ckSeqModeButton.Text = "+/- mode";
            Program.saveConfig();

        }

        private void CkSeqModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.CenterMode = "seq";
            this.ckAxisModeButton.Text = "Axis";
            this.ckDisableButton.Text = "Disable";
            this.ckIrFbModeButton.Text = "[x] " + this.ckSeqModeButton.Text;
            this.ckSeqModeButton.Text = "+/- mode";
            Program.saveConfig();

        }

        private void CkAxisModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.CenterMode = "axis";
            this.ckAxisModeButton.Text = "[x] " + this.ckAxisModeButton.Text;
            this.ckDisableButton.Text = "Disable";
            this.ckIrFbModeButton.Text = "iRacing Fbox";
            this.ckSeqModeButton.Text = "+/- mode";
            Program.saveConfig();

        }

        private void LkDisableButton_Click(object sender, EventArgs e)
        {
            Program.knobs.LeftMode = "disabled";
            this.lkDisableButton.Text = "[x] " + this.lkDisableButton.Text;
            this.lkAxisModeButton.Text = "Axis";
            this.lkIrFbModeButton.Text = "iRacing Fbox";
            this.lkSeqModeButton.Text = "+/- mode";
            Program.saveConfig();

        }

        private void LkIrFbModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.LeftMode = "irbb";
            this.lkDisableButton.Text = "Disable";
            this.lkAxisModeButton.Text = "Axis";
            this.lkIrFbModeButton.Text = "[x] " + this.lkIrFbModeButton.Text;
            this.lkSeqModeButton.Text = "+/- mode";
            Program.saveConfig();
        }

        private void LkSeqModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.LeftMode = "seq";
            this.lkDisableButton.Text = "Disable";
            this.lkAxisModeButton.Text = "Axis";
            this.lkIrFbModeButton.Text = "iRacing Fbox";
            this.lkSeqModeButton.Text = "[x] " + this.lkSeqModeButton.Text;
            Program.saveConfig();

        }

        private void LkAxisModeButton_Click(object sender, EventArgs e)
        {
            Program.knobs.LeftMode = "axis";
            this.lkDisableButton.Text = "Disable";
            this.lkAxisModeButton.Text = "[x] " + this.lkAxisModeButton.Text;
            this.lkIrFbModeButton.Text = "iRacing Fbox";
            this.lkSeqModeButton.Text = "+/- mode";
            Program.saveConfig();

        }

        private void runPreStartup()
        {
            String lkmode = Program.knobs.LeftMode;
            String ckmode = Program.knobs.CenterMode;
            String rkmode = Program.knobs.RightMode;

            if (lkmode.Equals("axis"))
                lkAxisModeButton.Text = "[x] " + lkAxisModeButton.Text;
            else if (lkmode.Equals("seq"))
                lkSeqModeButton.Text = "[x] " + lkSeqModeButton.Text;
            else if (lkmode.Equals("irbb"))
                lkIrFbModeButton.Text = "[x] " + lkIrFbModeButton.Text;
            else if (lkmode.Equals("disabled"))
                lkDisableButton.Text = "[x] " + lkDisableButton.Text;

            if (ckmode.Equals("axis"))
                ckAxisModeButton.Text = "[x] " + ckAxisModeButton.Text;
            else if (ckmode.Equals("seq"))
                ckSeqModeButton.Text = "[x] " + ckSeqModeButton.Text;
            else if (ckmode.Equals("irbb"))
                ckIrFbModeButton.Text = "[x] " + ckIrFbModeButton.Text;
            else if (ckmode.Equals("disabled"))
                ckDisableButton.Text = "[x] " + ckDisableButton.Text;

            if (rkmode.Equals("axis"))
                rkAxisModeButton.Text = "[x] " + rkAxisModeButton.Text;
            else if (rkmode.Equals("seq"))
                rkSeqModeButton.Text = "[x] " + rkSeqModeButton.Text;
            else if (rkmode.Equals("disabled"))
                rkDisableButton.Text = "[x] " + rkDisableButton.Text;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

    }


}
