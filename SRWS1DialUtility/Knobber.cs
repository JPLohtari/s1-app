﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mighty.HID;

namespace SRWS1DialUtility
{
    class Knobber
    {
        private static int left, oldleft, center, oldcenter, right, oldright;
        private static bool leftsent, centersent, rightsent;
        private static string leftMode, centerMode, rightMode;

        public Knobber()
        {
            leftMode = centerMode = rightMode = "";
            leftsent = centersent = rightsent = false;
            left = oldleft = center = oldcenter = right = oldright = 0;
;
        }

        public int Left
        {
            get
            {
                return left;
            }

            set
            {
                if(value != left)
                {
                    Oldleft = left;
                    left = value;
                    leftsent = false;
                }
            }
        }

        public int Oldleft
        {
            get
            {
                return oldleft;
            }

            set
            {
                if(oldleft != value)
                 oldleft = value;
            }
        }

        public int Center
        {
            get
            {
                return center;
            }

            set
            {
                if(value != center)
                {
                    Oldcenter = center;
                    center = value;
                    centersent = false;
                }
            }
        }

        public int Oldcenter
        {
            get
            {
                return oldcenter;
            }

            set
            {
                if(value != oldcenter)
                    oldcenter = value;
            }
        }

        public int Right
        {
            get
            {
                return right;
            }

            set
            {
                if(value != right)
                {
                    Oldright = right;
                    right = value;
                    rightsent = false;
                }
            }
        }

        public int Oldright
        {
            get
            {
                return oldright;
            }

            set
            {
                oldright = value;
            }
        }

        public bool Leftsent
        {
            get
            {
                return leftsent;
            }

            set
            {
                leftsent = value;
            }
        }

        public bool Centersent
        {
            get
            {
                return centersent;
            }

            set
            {
                centersent = value;
            }
        }

        public bool Rightsent
        {
            get
            {
                return rightsent;
            }

            set
            {
                if(value != rightsent)
                    rightsent = value;
            }
        }

        public string LeftMode
        {
            get
            {
                return leftMode;
            }

            set
            {
                if(value != leftMode)
                {
                    if(Program.vjs != null && (leftMode == "axis" && value != "axis"))
                    {
                        Program.vjs.ResetX = true;
                    }
                    leftMode = value;
                }
                
            }
        }

        public string CenterMode
        {
            get
            {
                return centerMode;
            }

            set
            {
                if (value != centerMode)
                {
                    if (Program.vjs != null && (centerMode == "axis" && value != "axis"))
                    {
                        Program.vjs.ResetY = true;
                    }
                    centerMode = value;
                }
            }
        }

        public string RightMode
        {
            get
            {
                return rightMode;
            }

            set
            {
                if (value != rightMode)
                {
                    if (Program.vjs != null && (rightMode == "axis" && value != "axis"))
                    {
                        Program.vjs.ResetZ = true;
                    }
                    rightMode = value;
                }
            }
        }

        public void monitor()
        {
            HIDInfo srinfo = null;
            HIDDev srwheel = null;
            var devs = HIDBrowse.Browse();
            byte[] data = new byte[30];
            foreach (var dev in devs)
            {
                if (dev.Product.Contains("SRWheel"))
                    srinfo = dev;
            }

            try
            {
                srwheel = new HIDDev();
                srwheel.Open(srinfo);
            }
            catch(NullReferenceException e)
            {
                System.Diagnostics.Debug.WriteLine("DEVNOTFOUND");
                throw new Exception("Device SRWHEEL not found!");
            }
            System.Diagnostics.Debug.WriteLine("Going for knob loop");
            while (true)
            {
                srwheel.fileStream.Read(data, 0, 30);
                int leftcenterValue = data[9];
                int rightvalue = data[10];
                Center = (leftcenterValue / 16);
                Left = leftcenterValue - (Center * 16);
                Right = rightvalue - 240;
                // System.Threading.Thread.Sleep(10);
            }
        }
    }
}
