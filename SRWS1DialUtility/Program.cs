﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using iRacingSdkWrapper;
using vJoyInterfaceWrap;
using Mighty.HID;
using System.Threading;
using System.Configuration;
using System.IO;
using WindowsInput;

namespace SRWS1DialUtility
{

    static class Program
    {
        static public Knobber knobs { get; set; }
        static public vJoyStick vjs { get; set; }
        static private Thread knobMonitor { get; set; }
        static private Thread jsFeeder { get; set; }
        static public Configuration appConfig { get; set; }
        static public bool iRRunning { get; set; }
        static private SdkWrapper irsdk { get; set; }
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            appConfig = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
            knobs = new Knobber();
            iRRunning = false;
            irsdk = new SdkWrapper();
            irsdk.Connected += Irsdk_Connected;
            irsdk.Disconnected += Irsdk_Disconnected;
            irsdk.Start();
            if (appConfig.AppSettings.Settings.AllKeys.Length == 0)
            {
                runFirstTimeConfig();
            }
            else
            {
                knobs.LeftMode = ConfigurationManager.AppSettings["leftKnob"];
                knobs.CenterMode = ConfigurationManager.AppSettings["centerKnob"];
                knobs.RightMode = ConfigurationManager.AppSettings["rightKnob"];
            }
            knobMonitor = new Thread(() => monitorKnobs());
            knobMonitor.Start();
            vjs = new vJoyStick();
            jsFeeder = new Thread(() => feedvJoy());
            jsFeeder.Start();

            Application.EnableVisualStyles();
            Application.ThreadException += Application_ThreadException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;
            Application.ThreadExit += Application_ThreadExit;
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Exception thrown, log should be created into your Documents-folder." + Environment.NewLine + (e.ExceptionObject as Exception).Message);
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\S1DialUtility_CrashReport-" + DateTime.Now.ToString() + "-crash.txt";
            using (StreamWriter wr = new StreamWriter(filePath, true))
            {
                wr.WriteLine((e.ExceptionObject as Exception).Message + Environment.NewLine + (e.ExceptionObject as Exception).StackTrace);
                wr.WriteLine("------------------------");
            }
            killApplication();
        }

        private static void Irsdk_Disconnected(object sender, EventArgs e)
        {
            iRRunning = false;
        }

        private static void Irsdk_Connected(object sender, EventArgs e)
        {
            iRRunning = true;
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show("Exception thrown, log should be created into your Documents-folder." + Environment.NewLine + e.Exception.Message);
            string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\S1DialUtility_CrashReport-" + DateTime.Now.ToString() + "-crash.txt";
            using (StreamWriter wr = new StreamWriter(filePath, true))
            {
                wr.WriteLine(e.Exception.Message + Environment.NewLine + e.Exception.StackTrace);
                wr.WriteLine("------------------------");
            }
            killApplication();
        }

        private static void runFirstTimeConfig()
        {
            appConfig.AppSettings.Settings.Add("leftKnob", "axis");
            appConfig.AppSettings.Settings.Add("centerKnob", "axis");
            appConfig.AppSettings.Settings.Add("rightKnob", "seq");
            knobs.LeftMode = "axis";
            knobs.CenterMode = "axis";
            knobs.RightMode = "seq";
            appConfig.Save(ConfigurationSaveMode.Minimal);
        }

        public static void saveConfig()
        {
            appConfig.AppSettings.Settings.Remove("leftKnob");
            appConfig.AppSettings.Settings.Remove("centerKnob");
            appConfig.AppSettings.Settings.Remove("rightKnob");
            appConfig.AppSettings.Settings.Add("leftKnob", knobs.LeftMode);
            appConfig.AppSettings.Settings.Add("centerKnob", knobs.CenterMode);
            appConfig.AppSettings.Settings.Add("rightKnob", knobs.RightMode);
            appConfig.Save(ConfigurationSaveMode.Minimal);
        }

        private static void Application_ThreadExit(object sender, EventArgs e)
        {
            knobMonitor.Abort();
            vjs.kill();
            jsFeeder.Abort();
        }

        public static void killApplication()
        {
            vjs.kill();
            knobMonitor.Abort();
            jsFeeder.Abort();
            irsdk.Stop();
            Application.Exit();
        } 

        private static void monitorKnobs()
        {
            knobs.monitor();
        }

        private static void feedvJoy()
        {
            vjs.startFeeding();
        }
    }
}
