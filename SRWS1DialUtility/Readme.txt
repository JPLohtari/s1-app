﻿SRW S1 Dial App

Since original SRW S1 Dial Utility is dead, I decided to try to write a new one. No warranty or anything.
The logo is from google material design site https://design.google.com/icons/

Requires latest vJoy-driver, get it here: http://vjoystick.sourceforge.net/site/
App currently uses the 1st vJoy-device and it has to have 8 buttons AND XYZ-axis enabled.

If you want to build this, you need:
iRacing SDK C# Wrapper by Nick Thissen
InputSimulator (from nuget or https://inputsimulator.codeplex.com/), Michael Noonan
MightyHID by Tomasz Watorowski, MightyDevices http://mightydevices.com/
vJoyInterfaceWrap and SDK (2.1.6), install wrapper as reference and add the vjoyinterface as a separate file.

-- Jari Lohtari