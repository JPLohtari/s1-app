﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vJoyInterfaceWrap;
using WindowsInput;

namespace SRWS1DialUtility
{
    class vJoyStick
    {
        static private vJoy js { get; set; }
        static private vJoy.JoystickState jsrep { get; set; }
        static private InputSimulator insim { get; set; }
        private static long xmaxval, ymaxval, zmaxval;
        private static uint id;
        private static bool result;
        private static bool update;
        private static bool resetZ;
        private static bool resetX;
        private static bool resetY;

        public vJoyStick() 
        {
            xmaxval = ymaxval = zmaxval = 0;
            resetX = resetY = resetZ = false;
            result = false;
            update = true;
            this.Id = 1;
            insim = new InputSimulator();
            js = new vJoy();
            jsrep = new vJoy.JoystickState();

            if (!js.vJoyEnabled() ||!js.AcquireVJD(id))
            {
                System.Diagnostics.Debug.WriteLine("vjd acquire failed");
                throw new Exception("vJoy id 1 in use? vJoy enabled? Failed to acquire.");
            }

            bool xAxis = js.GetVJDAxisExist(Id, HID_USAGES.HID_USAGE_X);
            bool yAxis = js.GetVJDAxisExist(Id, HID_USAGES.HID_USAGE_Y);
            bool zAxis = js.GetVJDAxisExist(Id, HID_USAGES.HID_USAGE_Z);
            int buttonCount = js.GetVJDButtonNumber(Id);
            if (!xAxis || !yAxis || !zAxis || buttonCount < 8)
            {
                System.Diagnostics.Debug.WriteLine("Button count fail.");
                throw new Exception("Not enough axis or buttons (XYZ axis and 8 buttons required.");
            }

            js.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_X, ref xmaxval);
            js.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_Y, ref ymaxval);
            js.GetVJDAxisMax(id, HID_USAGES.HID_USAGE_Z, ref zmaxval);

        }


        public long Xmaxval
        {
            get
            {
                return xmaxval;
            }

            set
            {
                xmaxval = value;
            }
        }

        public long Ymaxval
        {
            get
            {
                return ymaxval;
            }

            set
            {
                ymaxval = value;
            }
        }

        public long Zmaxval
        {
            get
            {
                return zmaxval;
            }

            set
            {
                zmaxval = value;
            }
        }

        public uint Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public bool Result
        {
            get
            {
                return result;
            }

            set
            {
                result = value;
            }
        }

        public bool ResetX
        {
            get
            {
                return resetX;
            }

            set
            {
                resetX = value;
            }
        }

        public bool ResetY
        {
            get
            {
                return resetY;
            }

            set
            {
                resetY = value;
            }
        }

        public bool ResetZ
        {
            get
            {
                return resetZ;
            }

            set
            {
                resetZ = value;
            }
        }


        public void startFeeding()
        {
            js.ResetVJD(Id);
            System.Diagnostics.Debug.WriteLine("JS Reset");
            int lk = 0, ck = 0, rk = 0, oldlk = 0, oldck = 0, oldrk = 0;
            string lkmode = "", ckmode = "", rkmode = "";
            bool lksent = false, cksent = false, rksent = false;
            System.Diagnostics.Debug.WriteLine("Going for feed loop");
            while (update)
            {
                // updateCurrentKnobState(lk, ck, rk, oldlk, oldck, oldrk, lkmode, ckmode, rkmode, lksent, cksent, rksent);

                lk = Program.knobs.Left;
                oldlk = Program.knobs.Oldleft;
                lksent = Program.knobs.Leftsent;
                lkmode = Program.knobs.LeftMode;

                ck = Program.knobs.Center;
                oldck = Program.knobs.Oldcenter;
                cksent = Program.knobs.Centersent;
                ckmode = Program.knobs.CenterMode;

                rk = Program.knobs.Right;
                oldrk = Program.knobs.Oldright;
                rksent = Program.knobs.Rightsent;
                rkmode = Program.knobs.RightMode;

                if(resetX)
                {
                    result = setAxis(0, HID_USAGES.HID_USAGE_X, id);
                    resetX = false;
                }
                else if(resetY)
                {
                    result = setAxis(0, HID_USAGES.HID_USAGE_Y, id);
                    resetY = false;
                }
                else if (resetZ)
                {
                    result = setAxis(0, HID_USAGES.HID_USAGE_Z, id);
                    resetZ = false;
                }

                if (lkmode.Equals("axis"))
                {
                    result = setAxis(lk, HID_USAGES.HID_USAGE_X, id);
                }
                else if (lkmode.Equals("seq") && !lksent)
                {
                    if (lk == oldlk + 1 || (lk == 0 && oldlk == 11))
                    {
                        // We want to go up.
                        pushButton(id, 2);
                    }
                    else if (lk == oldlk - 1 || (lk == 11 && oldlk == 0))
                    {
                        // We want to go down.
                        pushButton(id, 1);
                    }

                    Program.knobs.Leftsent = true;
                }
                else if (lkmode.Equals("irbb") && !lksent && Program.iRRunning)
                {
                    irFbMode(lk, oldlk);
                    Program.knobs.Leftsent = true;
                }



                if (ckmode.Equals("axis"))
                {
                    result = setAxis(ck, HID_USAGES.HID_USAGE_Y, id);
                }
                else if (ckmode.Equals("axis") && !cksent)
                {
                    if (ck == oldck + 1 || (ck == 0 && oldck == 11))
                    {
                        // We want to go up.
                        pushButton(id, 4);
                    }
                    else if (ck == oldck - 1 || (ck == 11 && oldck == 0))
                    {
                        // We want to go down.
                        pushButton(id, 3);
                    }

                    Program.knobs.Centersent = true;
                }
                else if (ckmode.Equals("irbb") && !cksent && Program.iRRunning)
                {
                    irFbMode(ck, oldck);
                    Program.knobs.Centersent = true;
                }

                if (rkmode.Equals("axis"))
                {
                    switch (rk)
                    {
                        case 0:
                            result = js.SetAxis(0, id, HID_USAGES.HID_USAGE_Z);
                            break;
                        case 1:
                            result = js.SetAxis(10000, id, HID_USAGES.HID_USAGE_Z);
                            break;
                        case 2:
                            result = js.SetAxis(20000, id, HID_USAGES.HID_USAGE_Z);
                            break;
                        case 3:
                            result = js.SetAxis(32767, id, HID_USAGES.HID_USAGE_Z);
                            break;
                    }
                }
                else if (rkmode.Equals("seq") && !rksent)
                {
                    if (rk == oldrk + 1 || (rk == 0 && oldrk == 11))
                    {
                        // We want to go up.
                        pushButton(id, 6);
                    }
                    else if (rk == oldrk - 1 || (rk == 11 && oldrk == 0))
                    {
                        // We want to go down.
                        pushButton(id, 5);
                    }

                    Program.knobs.Rightsent = true;
                }
                System.Threading.Thread.Sleep(10);

            }
        }

        private static bool pushButton(uint id, uint btn)
        {
            bool result;
            result = js.SetBtn(true, id, btn);
            System.Threading.Thread.Sleep(20);
            result = js.SetBtn(false, id, btn);
            return result;
        }

        private static bool setAxis(int knobState, HID_USAGES hid, uint id)
        {
            bool result = false;
            switch (knobState)
            {
                case 0:
                    result = js.SetAxis(0, id, hid);
                    break;
                case 1:
                    result = js.SetAxis(2979, id, hid);
                    break;
                case 2:
                    result = js.SetAxis(5958, id, hid);
                    break;
                case 3:
                    result = js.SetAxis(8937, id, hid);
                    break;
                case 4:
                    result = js.SetAxis(11916, id, hid);
                    break;
                case 5:
                    result = js.SetAxis(14895, id, hid);
                    break;
                case 6:
                    result = js.SetAxis(17874, id, hid);
                    break;
                case 7:
                    result = js.SetAxis(20853, id, hid);
                    break;
                case 8:
                    result = js.SetAxis(23832, id, hid);
                    break;
                case 9:
                    result = js.SetAxis(26811, id, hid);
                    break;
                case 10:
                    result = js.SetAxis(29790, id, hid);
                    break;
                case 11:
                    result = js.SetAxis(32767, id, hid);
                    break;
                default:
                    break;
            }

            return result;
        }

        private static void irFbMode(int newKnob, int oldKnob)
        {
            if (oldKnob == 11 && newKnob == 0)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F10);
            }
            else if (oldKnob == 1 && newKnob == 0)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F1);
            }
            else if (newKnob == 0)
            {

            }
            else if (newKnob == 1)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F1);
            }
            else if (newKnob == 2)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F2);
            }
            else if (newKnob == 3)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F3);
            }
            else if (newKnob == 4)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F4);
            }
            else if (newKnob == 5)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F5);
            }
            else if (newKnob == 6)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F6);
            }
            else if (newKnob == 7)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F7);
            }
            else if (newKnob == 8)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F8);
            }
            else if (newKnob == 9)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F9);
            }
            else if (newKnob == 10)
            {
                insim.Keyboard.KeyPress(WindowsInput.Native.VirtualKeyCode.F10);
            }
        }

        private static void updateCurrentKnobState(int lk, int ck, int rk, int oldlk, int oldck, int oldrk, string lkmode, string ckmode, string rkmode, bool lksent, bool cksent, bool rksent)
        {
            lk = Program.knobs.Left;
            oldlk = Program.knobs.Oldleft;
            lksent = Program.knobs.Leftsent;
            lkmode = Program.knobs.LeftMode;

            ck = Program.knobs.Center;
            oldck = Program.knobs.Oldcenter;
            cksent = Program.knobs.Centersent;
            ckmode = Program.knobs.CenterMode;

            rk = Program.knobs.Right;
            oldrk = Program.knobs.Oldright;
            rksent = Program.knobs.Rightsent;
            rkmode = Program.knobs.RightMode;
        }

        public void kill()
        {
            update = false;
            js.ResetVJD(id);
        }

    }
}
